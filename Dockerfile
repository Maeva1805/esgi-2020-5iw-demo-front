FROM node:16-alpine AS node

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm install

FROM nginx:1.21-alpine

COPY . /usr/share/nginx/html
COPY --from=node /app/node_modules /usr/share/nginx/html/node_modules

CMD sed -i "s/80/${PORT:-80}/g" /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'